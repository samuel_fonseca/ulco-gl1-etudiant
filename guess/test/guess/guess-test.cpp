#include <guess/guess.hpp>

#include <catch2/catch.hpp>

/// test de la fonction verif_new_nb
TEST_CASE( "test 1" ) {
    Jeu test;
    test.crea_nb_alea();
    int nb_test = test.demande_nb();
    std::string verif = test.verif_new_nb(nb_test);
    if(nb_test < test.value_choisie){
        REQUIRE( verif == "too low" );
    }
    else{
            REQUIRE(verif == "too high");
   
    }
}

///test si le nombre est bien compris entre 1 et 100
TEST_CASE("test 2") {
    Jeu test;
    test.crea_nb_alea();
    REQUIRE(test.value_choisie >= 1);
    REQUIRE(test.value_choisie <= 100);
}

