#pragma once

#include <iostream>
#include <string>
#include <vector>

//structure jeu
struct Jeu{

    ///tableau qui contient l'historique des entrees
    std::vector<int> history;
    ///valeur a trouver
    int value_choisie;

    ///constructeur de Jeu
    Jeu() : history(), value_choisie(0){};
    ///destructeur de Jeu
    ~Jeu() {};

    /// la fonction principale
    void run();

    ///fonction de demande du nouveaux nombre
    int demande_nb(); 

    ///fonction de verification du nombre entree
    std::string verif_new_nb(int);

    ///fonction d'ajout a l'historique
    void ajout_history(int);

    ///creation du nombre a trouver
    void crea_nb_alea();

    ///affiche le nombre a trouver
    void aff_target();

    ///fonction d'affichage de l'historique
    void aff_hisstory();

    ///fonction d'affichage du rapport entre le nombre entrer et le nombre a trouver
    void aff_verif(std::string);
    
};

