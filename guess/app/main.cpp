#include <guess/guess.hpp>

#include <iostream>

/// \mainpage Documentation de guess game
/// Implémentez un programme qui demande de deviner un nombre choisi aléatoirement entre 1 et 100. 
/// Le joueur a 5 essais maximum pour deviner le nombre. 
/// À chaque essai, le programme indique si le nombre donné est trop petit ou trop grand, ou si le jeu est gagné ou perdu.
///
/// pour lancer 

int main() {
    std::cout << "this is guess" << std::endl;
    Jeu new_jeu;
    new_jeu.run();
    return 0;
}

