#include <guess/guess.hpp>


void Jeu::crea_nb_alea(){

    srand (time(NULL));

    value_choisie = rand() % 100 +1;

}


void Jeu::ajout_history(int new_nb){

    history.push_back(new_nb);

}


std::string Jeu::verif_new_nb(int new_nb){

        if (new_nb < value_choisie) {
            return "too low";
        }
        else{
            return "too high";
        }
}



int Jeu::demande_nb(){
    int new_nb;

    std::cout<<"number? ";
    std::cin >> new_nb;

    return new_nb;
}


void Jeu::aff_hisstory(){

    std::cout<< "history : ";

    for(int i=0; i<history.size(); i++){

        std::cout<<history[i] << " ";
    }

    std::cout<< std::endl;

}


void Jeu::aff_verif(std::string verif){

    std::cout<< verif << std::endl;
}


void Jeu::aff_target(){

    std::cout<<"target : " << value_choisie;
}


void Jeu::run(){
    int essai = 5;
    crea_nb_alea();
    std::string verif;
    int new_nb;
    bool victoire = false;


    while(essai > 0){

        aff_hisstory();
        new_nb = demande_nb();
        if(new_nb == value_choisie ){
            
            victoire = true;
            break;
        }
        verif = verif_new_nb(new_nb);
        aff_verif(verif);
        ajout_history(new_nb);
        essai--;
    }

    if (victoire){
        std::cout << "you win";
    }
    else{
        std::cout << "you lose" << std::endl;
        aff_target();
    }
}
