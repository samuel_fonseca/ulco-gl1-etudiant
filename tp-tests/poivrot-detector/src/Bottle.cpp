#include "Bottle.hpp"

#include <sstream>

double alcoholVolume(const Bottle & b) {
    return b._deg * b._vol;
}

double alcoholVolume(const std::vector<Bottle> & bs) {
    double resultat = 0;

    for(int i =0 ; i <= bs.size(); i++)
    {
        resultat = resultat + (bs[i]._deg * bs[i]._vol);

    };

    return resultat;
}

bool isPoivrot(const std::vector<Bottle> & bs) {
    
    double volume = alcoholVolume(bs);
    
    if (volume > 0.1) {

        return true;
    }
    else{

        return false;
    }
}

std::vector<Bottle> readBottles(std::istream & is) {
    // TODO
    return {};
}

