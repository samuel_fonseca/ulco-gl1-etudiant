#include "../src/Bottle.hpp"

#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE( "init Chimay" ) {
    const Bottle b {"Chimay", 0.75, 0.08};
    REQUIRE(b._name == "Chimay");
    REQUIRE(b._vol == 0.75);
    REQUIRE(b._deg == 0.08);
}

TEST_CASE( "init Orange Pur Jus" ) {
    const Bottle b {"Orange Pur Jus", 1, 0};
    REQUIRE(b._name == "Orange Pur Jus");
    REQUIRE(b._vol == 1);
    REQUIRE(b._deg == 0);
}

TEST_CASE( "alcoholVolume Chimay" ) {
    const Bottle b {"Chimay", 0.75, 0.08};
    double resultat = alcoholVolume(b);
    REQUIRE ( resultat == 0.06);
}

TEST_CASE( "alcoholVolume Orange Pur Jus" ) {
    const Bottle b {"Orange Pur Jus", 1, 0};
    double resultat = alcoholVolume(b);
    REQUIRE ( resultat == 0);

}

TEST_CASE( "alcoholVolume Chimay + Orange" ) {
    std::vector<Bottle>liste;
    const Bottle b1 {"Chimay", 0.75, 0.08};
    const Bottle b2 {"Orange Pur Jus", 1, 0};

    liste.push_back(b1);
    liste.push_back(b2);

    double resultat = alcoholVolume(liste);

    REQUIRE( resultat == 0.06);
}

TEST_CASE( "alcoholVolume Chimay + Chimay" ) {
    std::vector<Bottle>liste;
    const Bottle b1 {"Chimay", 0.75, 0.08};
    const Bottle b2 {"Chimay", 0.75, 0.08};

    liste.push_back(b1);
    liste.push_back(b2);
    
    double resultat = alcoholVolume(liste);

    REQUIRE( resultat == 0.12);
}

TEST_CASE( "isPoivrot Chimay + Orange" ) {
    std::vector<Bottle>liste;
    const Bottle b1 {"Chimay", 0.75, 0.08};
    const Bottle b2 {"Orange Pur Jus", 1, 0};

    liste.push_back(b1);
    liste.push_back(b2);
    
    double resultat = isPoivrot(liste);

    REQUIRE( resultat == false);
}

TEST_CASE( "isPoivrot Chimay + Chimay" ) {
    std::vector<Bottle>liste;
    const Bottle b1 {"Chimay", 0.75, 0.08};
    const Bottle b2 {"Chimay", 0.75, 0.08};

    liste.push_back(b1);
    liste.push_back(b2);
    
    double resultat = isPoivrot(liste);

    REQUIRE( resultat == true);
}

TEST_CASE( "readBottles Chimay" ) {
    // TODO
}

TEST_CASE( "readBottles Chimay + Orange" ) {
    // TODO
}

