#include <cesar/cesar.hpp>

#include <catch2/catch.hpp>

TEST_CASE( "test chiffrement lettre " ) {
    REQUIRE( chiffrer_lettre('a', 2) == 'c' );
}

TEST_CASE( "test chiffrement lettre erreur" ) {
    REQUIRE( chiffrer_lettre('A', 2) == 'A' );
}


TEST_CASE( "test chiffrement msg" ) {
    REQUIRE( chiffrer_str("aaaa", 2) == "cccc" );
}

TEST_CASE( "test chiffrement msg erreur" ) {
    REQUIRE( chiffrer_str("aaaA", 2) == "cccA" );
}

TEST_CASE( "test dechiffrement lettre" ) {
    REQUIRE( dechiffrer_lettre('a', 2) == 'y' );
}

TEST_CASE( "test dechiffrement lettre erreur" ) {
    REQUIRE( dechiffrer_lettre('A', 2) == 'A' );
}

TEST_CASE( "test dechiffrement msg" ) {
    REQUIRE( dechiffrer_str("aaaa", 2) == "yyyy" );
}

TEST_CASE( "test dechiffrement msg erreur" ) {
    REQUIRE( dechiffrer_str("aaaA", 2) == "yyyA" );
}