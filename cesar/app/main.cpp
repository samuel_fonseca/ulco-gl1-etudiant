#include <cesar/cesar.hpp>

#include <iostream>

int main() {

    std::cout << chiffrer_lettre('z', 3) << std::endl;
    std::cout << chiffrer_str("cccc", 2) << std::endl;
    std::cout << dechiffrer_lettre('b', 3) << std::endl;
    std::cout << dechiffrer_str("cccc", 5) << std::endl;
    
    //recuperation de texte dans le fichiers
    std::string str;
    std::string line;
    while (std::getline(std::cin, line))
    {
        str += line + "\n";
    }
    std::cout << str << std::endl;    
    
    //test tout dechiffrement possible

    std::vector<std::string> stockage;
    stockage = stockage_possiblite_txt(str);

    for(int i=0; i<stockage.size();i++){
        std::cout<< "essai "<< i+1 <<": [";
       std::vector<float> freqs = calcul_freq(stockage[i]);
       for(int j=0; j<freqs.size(); j++){
           std::cout << freqs[j] << " ";
       }
       std::cout << "]" <<std::endl;

       std::cout<< stockage[i]<< std::endl;

    }

    return 0;
}

