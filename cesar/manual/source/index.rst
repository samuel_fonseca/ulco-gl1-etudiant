.. cesar documentation master file, created by
   sphinx-quickstart on Thu Dec  3 15:56:09 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cesar's documentation!
=================================

Bienvenue sur le page du projet qui porte sur le chiffrement de Cesar.

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
