#include <cesar/cesar.hpp>

char chiffrer_lettre(char lettre, int key){
    if(islower(lettre)){
        if(islower(lettre + key)){
            return lettre +key;  
        }
        else{
            return ((((int(lettre) - 97) + key) % 26) + 97);
        }  
    }
    else{
        return lettre;
    }
    
}

std::string chiffrer_str(std::string message, int key){
    std::string message_chiffer;
    for(int i= 0; i< message.size(); i++){
        message_chiffer.push_back(chiffrer_lettre(message[i], key));
    }

    return message_chiffer;

}

char dechiffrer_lettre(char lettre, int key){
    if(islower(lettre)){
        if(chiffrer_lettre(lettre, key) == islower(lettre)){
            return chiffrer_lettre(lettre, -key);
        }
        else{
            return chiffrer_lettre(lettre, 26-key);
        }
    }
    else{
        return lettre;
    }
}

std::string dechiffrer_str(std::string message, int key){
    std::string message_dechiffer;

    for(int i= 0; i< message.size(); i++){
        message_dechiffer.push_back(dechiffrer_lettre(message[i], key));
    }

    return message_dechiffer;

}

std::vector<std::string> stockage_possiblite_txt(std::string str){
    std::vector<std::string> stockage;

    for(int i=0; i<26; i++) {
    stockage.push_back(dechiffrer_str(str, i));
     
    }
    return stockage;
}

std::vector<float> calcul_freq(std::string str){
    std::vector<float> freqs;
    for(char j='a'; j<= 'z'; j++ ){
        float occ = 0;
        for(int i=0; i<str.size();i++){
                if(j == str[i]){
                    occ++;
                }
        }
        freqs.push_back(occ/str.size());
    }
    return freqs;
}