#pragma once

#include <string>
#include <ctype.h>
#include <vector>

const std::vector<double> refFreqs {
    0.082, 0.015, 0.028, 0.043, 0.127, 0.022, 0.02, 0.061, 0.07,
    0.002, 0.008, 0.04, 0.024, 0.067, 0.075, 0.019, 0.001, 0.06,
    0.063, 0.091, 0.028, 0.01, 0.024, 0.002, 0.02, 0.001
};

char chiffrer_lettre(char, int);

std::string chiffrer_str(std::string, int);


char dechiffrer_lettre(char, int);

std::string dechiffrer_str(std::string, int);

std::vector<std::string> stockage_possiblite_txt(std::string);

std::vector<float> calcul_freq(std::string);