#include "MyWindow.hpp"
#include <glog/logging.h>

int main(int argc, char ** argv) {
    google::InitGoogleLogging(argv[0]);
    LOG(INFO) << " teste des logs main";

    Gtk::Main kit(argc, argv);
    MyWindow window;
    kit.run(window);
    return 0;
}

